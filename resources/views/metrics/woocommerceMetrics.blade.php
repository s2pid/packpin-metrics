@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="panel panel-default">
            <div class="panel-heading">Metrics</div>
            {{--<form action="" method="post">--}}
                {{--{{csrf_field()}}--}}
                {{--<label style="padding-left:15px;">Year:&nbsp;--}}
                    {{--<select name="date" id="date">--}}
                        {{--@for($i = $yearOption['yearFrom']; $i <= $yearOption['yearTo']; $i++)--}}
                            {{--<option value="{{$i}}" selected>{{$i}}</option>--}}
                        {{--@endfor--}}
                    {{--</select>--}}
                    {{--<input type="submit" name="submit" value="Submit"/>--}}
                {{--</label>--}}
            {{--</form>--}}

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <h3 style="text-align:center">Woocommerce Metrics</h3>
                            <th></th>
                            @for($i = 1; $i <= 12; $i++)
                                <th>{{ $year }} {{ $i }}</th>
                            @endfor
                            <th>Total {{ $year }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($metrics as $metricTypeId => $metricType)
                            <tr class="info">
                                <td colspan="14">
                                    <strong>{{ array_get(\App\Models\Metric::getTypes(), $metricTypeId) }}</strong></td>
                            </tr>
                            @foreach($metricType as $metricId => $metricMonth)
                                <tr>
                                    <td>{{ $metricId }}</td>
                                    @foreach($metricMonth as $result)
                                        <td>{{ $result or 0 }}</td>
                                    @endforeach
                                </tr>
                                @if(count(array_get($metricMonth, 'growth', [])) > 0)
                                    <tr>
                                        <td>{{ $metricId }} Growth</td>
                                        @foreach($metricMonth as $result)
                                            <td>{{ $result or 0 }}</td>
                                        @endforeach
                                    </tr>
                                @endif
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
