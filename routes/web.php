<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/metrics', 'MetricsController@index');
Route::post('/metrics', 'MetricsController@index');
Route::get('/magentometrics', 'MetricsController@MagentoMetrics');
Route::post('/magentometrics', 'MetricsController@MagentoMetrics');
Route::get('/woocommercemetrics', 'MetricsController@WoocommerceMetrics');
Route::post('/woocommercemetrics', 'MetricsController@WoocommerceMetrics');

