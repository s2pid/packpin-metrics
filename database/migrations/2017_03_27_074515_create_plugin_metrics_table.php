<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePluginMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plugin_metrics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plugin');
            $table->string('metric');
            $table->integer('type')->unsigned()->default(0);
            $table->smallInteger('year');
            $table->tinyInteger('month');
            $table->integer('count')->default(0);
            $table->string('countType', 10)->default('number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plugin_metrics');
    }
}
