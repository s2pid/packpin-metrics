<?php

namespace App\Http\Controllers;

use App\Components\Metrics;
use App\Models\Metric;
use App\Models\MagentoMetrics;
use App\Models\PluginMetrics;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class MetricsController extends Controller
{
    /**
     * Create a new controller instance.
     {
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $dt = Carbon::now();
        $yearOption = ['yearFrom' => 2014, 'yearTo' => $dt->year];



        $year = Input::get('date');

        if(empty($year)) {
            $year = $dt->year;
        } else {
            $year = Input::get('date');
        }

        $metrics = Metric::where('year', $year)->orderBy('month', 'asc')->get();
        $m = new Metrics($metrics);

        return view('metrics.index', [
            'yearOption' => $yearOption,
            'year' => $year,
            'metrics' => $m->formatMetrics()
        ]);
    }



    public function MagentoMetrics() {
        $dt = Carbon::now();
        $yearOption = ['yearFrom' => 2014, 'yearTo' => $dt->year];



        $year = Input::get('date');

        if(empty($year)) {
            $year = $dt->year;
        } else {
            $year = Input::get('date');
        }

        $MagentoMetrics = PluginMetrics::where('year', $year)->where('plugin','magento')->orderBy('month','asc')->get();
        $m = new Metrics($MagentoMetrics);

        return view('metrics.magentoMetrics', [
            'yearOption' => $yearOption,
            'year' => $year,
            'metrics' => $m->formatMetrics()
        ]);
    }

    public function WoocommerceMetrics() {
        $dt = Carbon::now();
        $yearOption = ['yearFrom' => 2014, 'yearTo' => $dt->year];



        $year = Input::get('date');

        if(empty($year)) {
            $year = $dt->year;
        } else {
            $year = Input::get('date');
        }

        $WoocommerceMetrics = PluginMetrics::where('year', $year)->where('plugin','woocommerce')->orderBy('month','asc')->get();
        $m = new Metrics($WoocommerceMetrics);

        return view('metrics.woocommerceMetrics', [
            'yearOption' => $yearOption,
            'year' => $year,
            'metrics' => $m->formatMetrics()
        ]);
    }
}
