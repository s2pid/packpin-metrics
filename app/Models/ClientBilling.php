<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class ClientBilling extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_billing';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

}