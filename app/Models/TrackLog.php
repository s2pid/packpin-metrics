<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TrackLog extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'track_log';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

}