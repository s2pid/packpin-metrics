<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PluginMetrics extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plugin',
        'metric',
        'type',
        'year',
        'month',
        'count',
        'countType',
    ];

    const TOTAL_NEW_SIGNUPS = 'total_new_signups';

    const CUSTOMERS_BEGINNING_OF_THE_MONTH = 'Customers beginning of the month';
    const CUSTOMERS_END_OF_THE_MONTH = 'Customers end of the month';
    const CUSTOMERS_NEW = 'New customers';
    const CUSTOMERS_LOST = 'Lost customers';

    const MRR_BEGINNING_OF_THE_MONTH = 'MRR beginning of the month';
    const MRR_END_OF_THE_MONTH = 'MRR end of the month';
    const MRR_NEW_FROM_NEW_CUSTOMERS = 'New MRR from new customers';
    const MRR_NEW_FROM_ACCOUNT_EXPANSIONS = 'New MRR from account expansions';
    const MRR_LOST = 'Lost MRR';

    const EMAILS_SENT = 'Emails sent';
    const EMAILS_CLICKED = 'Emails clicked';
    const EMAILS_OPENED = 'Emails opened';
    const EMAILS_BOUNCED = 'Emails bounced';
    const TRACKING_NUMBERS = 'Tracking numbers in total';

    /**
     * Metrics type (category).
     *
     * @var array
     */
    protected static $types = [
        '1' => 'Visitors & signups',
        '2' => 'Paying customers',
        '3' => 'MRR',
        '4' => 'Other',
    ];

    /**
     * Get types
     *
     * @return array
     */
    public static function getTypes()
    {
        return self::$types;
    }
}
