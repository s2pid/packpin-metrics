<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ClientStats extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_stats';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

    /**
     * @var array
     */
    protected $dates = [
        'upgraded_to_paid'
    ];

}