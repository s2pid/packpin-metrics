<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ApiRequestLogs extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_request_log';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

}