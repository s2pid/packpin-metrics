<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ClientPayments extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_payments';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

}