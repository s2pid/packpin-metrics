<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ClientSales extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_sales';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

}