<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientConnectors extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_api_key_connectors';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';
}