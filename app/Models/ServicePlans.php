<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ServicePlans extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_plans';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

}