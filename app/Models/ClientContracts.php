<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ClientContracts extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'client_contracts';

    /**
     * Connection type
     *
     * @var string
     */
    protected $connection = 'mysqlRemote';

}