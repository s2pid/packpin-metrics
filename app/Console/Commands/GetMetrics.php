<?php

namespace App\Console\Commands;

use App\Models\ApiRequestLogs;
use App\Models\Client;
use App\Models\ClientBilling;
use App\Models\ClientConnectors;
use App\Models\ClientContracts;
use App\Models\ClientPayments;
use App\Models\ClientSales;
use App\Models\ClientStats;
use App\Models\Metric;
use App\Models\ServicePlans;
use App\Models\TrackLog;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mailgun\Mailgun;
use App\Models\PluginMetrics;
use Symfony\Component\Console\Input\InputOption;

class GetMetrics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'metrics:get {action?}';
    // new var to take only this month
    // new table with plugins

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collect metrics';

    protected $pluginMagento = false;
    protected $pluginWoocommerce = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    //if true take only this months data

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $action = $this->argument();

        if ($action['action'] == 'total') {

            $this->info('Getting info about all metrics');
            $this->getTotalSignups();
            $this->getCustomersNew();
            //$this->getCustomersLost();
            $this->getCustomersEndOfTheMonth();
            $this->getCustomersBeginningOfTheMonth();
            $this->getNewMRRFromNewCustomers();
            $this->getNewMRRFromAccountExpansions();
            $this->getMRREndOfTheMonth();
            $this->getMRRBeginningOfTheMonth();
            $this->getLostMRR();
            $this->getMailgunEmailsSent();
            $this->getMailgunEmailsOpened();
            $this->getMailgunEmailsClicked();
            $this->getMailgunEmailsBounced();
            //$this->getTrackingNumbers();

        } elseif ($action['action'] == 'magento') {

            $this->info('Getting info about Magento clients');
            $this->pluginMagento = true;
            $this->getNewMRRFromNewCustomers();
            //$this->getCustomersEndOfTheMonth();
            //$this->getCustomersNew();
            //$this->getMRREndOfTheMonth();

        } elseif ($action['action'] == 'woocommerce') {

            $this->info('Getting info about Woocommerce clients');
            $this->pluginWoocommerce = true;
            //$this->getNewMRRFromNewCustomers();

            //this->getCustomersEndOfTheMonth();
            //$this->getCustomersNew();
            //$this->getMRREndOfTheMonth();

        } else {
            $this->error('Please provide valid action from the list:');
            $this->info('php artisan ' . $action['command'] . ' total');
            $this->info('php artisan ' . $action['command'] . ' magento');
            $this->info('php artisan ' . $action['command'] . ' woocommerce');
        }
    }


    protected function getMailgunEmailsSent()
    {

        $mgClient = new Mailgun('key-9f0668d96db8893891e009e133e80cff');
        $domain = 'mg.packpin.com';

        # Issue the call to the client.
        $result = $mgClient->get("$domain/stats/total", array(
            'event' => array('delivered'),
            'duration' => '12m'
        ));

        $this->info('Get total emails sent started.');
        $bar = $this->output->createProgressBar(sizeof($result->http_response_body->stats));

        for ($i = 0; $i < sizeof($result->http_response_body->stats); $i++) {
            $year = date('Y', strtotime($result->http_response_body->stats[$i]->time));
            $month = date('m', strtotime($result->http_response_body->stats[$i]->time));
            $count = $result->http_response_body->stats[$i]->delivered->total;
            $bar->advance();

            $dt = Carbon::now();

            if ($month == $dt->month && $year == $dt->year) {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_SENT,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => 0]
                );
            } else {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_SENT,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => $count]
                );
            }
        }
            $bar->finish();
            $this->info('Get total emails sent ended.');

    }


    protected function getMailgunEmailsOpened()
    {

        $mgClient = new Mailgun('key-9f0668d96db8893891e009e133e80cff');
        $domain = 'mg.packpin.com';

        # Issue the call to the client.
        $result = $mgClient->get("$domain/stats/total", array(
            'event' => array('opened'),
            'duration' => '12m'
        ));

        $this->info('Get total emails opened started.');
        $bar = $this->output->createProgressBar(sizeof($result->http_response_body->stats));

        for ($i = 0; $i < sizeof($result->http_response_body->stats); $i++) {
            $year = date('Y', strtotime($result->http_response_body->stats[$i]->time));
            $month = date('m', strtotime($result->http_response_body->stats[$i]->time));
            $count = $result->http_response_body->stats[$i]->opened->total;
            $bar->advance();

            $dt = Carbon::now();

            if ($month == $dt->month && $year == $dt->year) {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_OPENED,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => 0]
                );
            } else {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_OPENED,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => $count]
                );
            }
        }
            $bar->finish();
            $this->info('Get total emails opened ended.');
    }

    protected function getMailgunEmailsClicked() {

        $mgClient = new Mailgun('key-9f0668d96db8893891e009e133e80cff');
        $domain = 'mg.packpin.com';

        # Issue the call to the client.
        $result = $mgClient->get("$domain/stats/total", array(
            'event' => array('clicked'),
            'duration' => '12m'
        ));

        $this->info('Get total emails clicked started.');
        $bar = $this->output->createProgressBar(sizeof($result->http_response_body->stats));

        for($i = 0; $i < sizeof($result->http_response_body->stats); $i++) {
            $year = date('Y', strtotime($result->http_response_body->stats[$i]->time));
            $month = date('m', strtotime($result->http_response_body->stats[$i]->time));
            $count = $result->http_response_body->stats[$i]->clicked->total;
            $bar->advance();

            $dt = Carbon::now();

            if ($month == $dt->month && $year == $dt->year) {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_CLICKED,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => 0]
                );
            } else {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_CLICKED,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => $count]
                );
            }
        }
        $bar->finish();
        $this->info('Get total emails clicked ended.');

    }


    protected function getMailgunEmailsBounced() {

        $mgClient = new Mailgun('key-9f0668d96db8893891e009e133e80cff');
        $domain = 'mg.packpin.com';

        # Issue the call to the client.
        $result = $mgClient->get("$domain/stats/total", array(
            'event' => array('failed'),
            'duration' => '12m'
        ));

        $this->info('Get total emails bounced started.');
        $bar = $this->output->createProgressBar(sizeof($result->http_response_body->stats));

        for($i = 0; $i < sizeof($result->http_response_body->stats); $i++) {
            $year = date('Y', strtotime($result->http_response_body->stats[$i]->time));
            $month = date('m', strtotime($result->http_response_body->stats[$i]->time));
            $count = $result->http_response_body->stats[$i]->failed->permanent->bounce;
            $bar->advance();

            $dt = Carbon::now();

            if ($month == $dt->month && $year == $dt->year) {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_BOUNCED,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => 0]
                );
            } else {
                Metric::updateOrCreate(
                    [
                        'metric' => Metric::EMAILS_BOUNCED,
                        'type' => 4,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => $count]
                );
            }
        }
        $bar->finish();
        $this->info('Get total emails bounced ended.');

    }

    /**
     * All users who signed up
     * select * from `pp_clients` order by `created_at` asc
     */
    protected function getTotalSignups()
    {
        $metrics = [];
        $query = Client::select('created_at')
            ->orderBy('created_at', 'asc');

        if($this->pluginMagento == true) {

            $query = ApiRequestLogs::select('created_at')->where('plugin_type', 'magento')->orderBy('created_at', 'asc')->limit(1);
            dd($query);
        }

        if($this->pluginWoocommerce == true) {
            $query = ClientConnectors::where('type', 'woocommerce')->groupby('client_id')->orderBy('created_at', 'asc');
        }

        $this->info('Get total signups started.');
        $bar = $this->output->createProgressBar($query->count());

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $metrics[$client->created_at->year][$client->created_at->month] += 1;
                $bar->advance();
            }
        });

        $bar->finish();


        // Update or create metric
        if($this->pluginMagento == false && $this->pluginWoocommerce == false) {
            $this->writeMetrics($metrics, Metric::TOTAL_NEW_SIGNUPS, 1);
        }
        if($this->pluginMagento == true) {
            $this->writePluginMetrics('magento', $metrics, PluginMetrics::TOTAL_NEW_SIGNUPS, 1);
        }
        if($this->pluginWoocommerce == true) {
            $this->writePluginMetrics('woocommerce', $metrics, PluginMetrics::TOTAL_NEW_SIGNUPS, 1);
        }
        $this->info('Get total signups ended.');
    }

    /**
     * This is a new customers who made a payment for the first time
     * select * from `pp_client_payments` where `transfer_id` is not null group by `client_id` order by `created_at` asc
     */
    protected function getCustomersNew()
    {
        $PluginClients = [];
        $metrics = [];
        $query = ClientPayments::select('created_at')
            ->whereNotNull('transfer_id')
            ->groupBy('client_id')
            ->orderBy('created_at');

        if($this->pluginMagento == true) {
            $PluginClient = ApiRequestLogs::select('created_at', 'client_id')->where('plugin_type', 'magento')->where('error', 'not like', 'Free%')->groupby('client_id')->orderby('created_at', 'asc')->get();

            foreach($PluginClient as $MClient) {
                $PluginClients[] = $MClient->client_id;
            }
            $query = ClientPayments::select('created_at')->whereNotNull('transfer_id')->whereIn('client_id', $PluginClients)->groupBy('client_id')->orderBy('created_at', 'asc');
        }

        if($this->pluginWoocommerce == true) {
            $PluginClient = ApiRequestLogs::select('created_at', 'client_id')->where('plugin_type', 'woocommerce')->where('error', 'not like', 'Free%')->orderby('created_at', 'asc')->groupby('client_id')->get();

            foreach($PluginClient as $WClient) {
                $PluginClients[] = $WClient->client_id;
            }
            $query = ClientPayments::select('created_at')->whereNotNull('transfer_id')->whereIn('client_id', $PluginClients)->groupBy('client_id')->orderBy('created_at', 'asc');
        }

        $this->info('Get new customers started.');
        $bar = $this->output->createProgressBar($query->count());

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $metrics[$client->created_at->year][$client->created_at->month] += 1;
                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric
        if($this->pluginMagento == false && $this->pluginWoocommerce == false) {
            $this->writeMetrics($metrics, Metric::CUSTOMERS_NEW, 2);
        }
        if($this->pluginMagento == true) {
            $this->writePluginMetrics('magento', $metrics, PluginMetrics::CUSTOMERS_NEW, 2);
        }
        if($this->pluginWoocommerce == true) {
            $this->writePluginMetrics('woocommerce', $metrics, PluginMetrics::CUSTOMERS_NEW, 2);
        }
        $this->info('Get new customers ended.');
    }

    /**
     * STATUS_CANCELED_INTENTIONALLY = 5
     * select `updated_at` from `pp_client_contracts` where `status` = 5 order by `updated_at` asc
     */
    protected function getCustomersLost()
    {
        $metrics = [];
        $query = ClientContracts::select('updated_at')
            ->where('status', 5)
            ->orderBy('updated_at', 'asc');

        $this->info('Get lost customers started.');
        $bar = $this->output->createProgressBar($query->count());

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                //$this->info($client->updated_at->toDateString());
                if (empty($metrics[$client->updated_at->year])) {
                    $metrics[$client->updated_at->year] = [];
                }
                if (empty($metrics[$client->updated_at->year][$client->updated_at->month])) {
                    $metrics[$client->updated_at->year][$client->updated_at->month] = 0;
                }

                $metrics[$client->updated_at->year][$client->updated_at->month] -= 1;
                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric
        $this->writeMetrics($metrics, Metric::CUSTOMERS_LOST, 2);
        $this->info('Get lost customers ended.');
    }


    /**
     * Getting all paid customers at the end of the month.
     * it's splitting everything in 2 last months, need to get only last month with all customers instead of 2 months
     * should be magento with errors 17 without 16, woocommerce with errors 10 without 6
     * check others for correct info.
     */
    protected function getCustomersEndOfTheMonth() {

        $PluginClients = [];
        $metrics = [];
        $query = ClientBilling::select('created_at')
            ->where('amount', '>', 0)
            ->orderBy('created_at', 'asc');


        if($this->pluginMagento == true) {

            $PluginClient = ApiRequestLogs::whereRaw('created_at = (select max(`created_at`))')->where('plugin_type', 'magento')->groupby('client_id')->orderby('created_at', 'asc')->get();

            foreach($PluginClient as $MClient) {
                $PluginClients[] = $MClient->client_id;
            }

            $query = ClientSales::where('active', '>', 0)->where('features_from_plan', '>', 1)->whereIn('client_id',$PluginClients)->orderBy('created_at', 'asc');
        }

        if($this->pluginWoocommerce == true) {

            $PluginClient = ApiRequestLogs::where('plugin_type', 'woocommerce')->groupby('client_id')->orderby('created_at', 'asc')->get();

            foreach($PluginClient as $WClient) {
                $PluginClients[] = $WClient->client_id;
            }

            $query = ClientSales::where('active', '>', 0)->where('features_from_plan', '>', 1)->whereIn('client_id',$PluginClients)->orderBy('created_at', 'asc');
        }

        $this->info('Get customers of the month started.');
        $bar = $this->output->createProgressBar($query->count());

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $metrics[$client->created_at->year][$client->created_at->month] += 1;

                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric
        if($this->pluginMagento == false && $this->pluginWoocommerce == false) {
            $this->writeMetrics($metrics, Metric::CUSTOMERS_END_OF_THE_MONTH, 2);
        }
        if($this->pluginMagento == true) {
            $this->writePluginMetrics('magento', $metrics, PluginMetrics::CUSTOMERS_END_OF_THE_MONTH, 2);
        }
        if($this->pluginWoocommerce == true) {
            $this->writePluginMetrics('woocommerce', $metrics, PluginMetrics::CUSTOMERS_END_OF_THE_MONTH, 2);
        }
        $this->info('Customers end of the month ended');
    }

    /**
     * Getting all paid customers at the beginning of the month
     * by taking last month customers at the end of the month.
     */
    protected function getCustomersBeginningOfTheMonth() {

        $metrics = [];
        $query = ClientBilling::select('created_at')
            ->where('amount', '>', 0)
            ->orderBy('created_at', 'asc');

        $this->info('Get customers beginning of the month started.');
        $bar = $this->output->createProgressBar($query->count());

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $beginYear = $client->created_at->year;
                $beginMonth = $client->created_at->month;

                if(isset($metrics[$beginYear][$beginMonth])) {

                    $metrics[$beginYear][$beginMonth] += 1;
                }


                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric
        $this->writeMetrics($metrics, Metric::CUSTOMERS_BEGINNING_OF_THE_MONTH, 2, true);
        $this->info('Customers beginning of the month ended');
    }



    /**
     * Gets only new client payments
     */
    protected function getNewMRRFromNewCustomers()
    {
        $PluginClients = [];
        $metrics = [];
        $query = ClientPayments::select('amount', 'created_at')
            ->whereNotNull('transfer_id')
            ->groupBy('client_id')
            ->orderBy('created_at');

        if($this->pluginWoocommerce) {
            $PluginClient = ApiRequestLogs::where('plugin_type', 'woocommerce')->groupby('client_id')->orderby('created_at', 'asc')->get();

            foreach($PluginClient as $WClient) {
                $PluginClients[] = $WClient->client_id;
            }

            $query = ClientPayments::select('amount', 'created_at')->whereNotNull('transfer_id')->whereIn('client_id', $PluginClients)->groupBy('client_id')->orderBy('created_at');

        }

        if($this->pluginMagento) {
            $PluginClient = ApiRequestLogs::where('plugin_type', 'magento')->groupby('client_id')->orderby('created_at', 'asc')->get();

            foreach($PluginClient as $WClient) {
                $PluginClients[] = $WClient->client_id;
            }

            $query = ClientPayments::select('amount', 'created_at')->whereNotNull('transfer_id')->whereIn('client_id', $PluginClients)->groupBy('client_id')->orderBy('created_at');

        }



        $this->info('Get new MRR from new customers started.');
        $bar = $this->output->createProgressBar($query->count());

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $metrics[$client->created_at->year][$client->created_at->month] += $client->amount;
                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric

        if($this->pluginMagento == false && $this->pluginWoocommerce == false) {
            $this->writeMetrics($metrics, Metric::MRR_NEW_FROM_NEW_CUSTOMERS, 3, false, true);
        }
        if($this->pluginWoocommerce) {
            $this->writePluginMetrics('woocommerce', $metrics, PluginMetrics::MRR_NEW_FROM_NEW_CUSTOMERS, 3);
        }
        if($this->pluginMagento) {
            $this->writePluginMetrics('magento', $metrics, PluginMetrics::MRR_NEW_FROM_NEW_CUSTOMERS, 3);
        }
        $this->info('Get new MRR from new customers ended.');
    }

    /**
     * Get upgraded client plans
     */
    protected function getNewMRRFromAccountExpansions()
    {
        $metrics = [];
        $query = ClientStats::select('upgraded_to_paid', 'upgraded_plan_id')
            ->whereNotNull('upgraded_to_paid')
            ->orderBy('upgraded_to_paid');

        $this->info('Get new MRR from account expansions started.');
        $bar = $this->output->createProgressBar($query->count());

        // Get plans price
        $plans = ServicePlans::pluck('price', 'id');


        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $plans, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->upgraded_to_paid->year])) {
                    $metrics[$client->upgraded_to_paid->year] = [];
                }
                if (empty($metrics[$client->upgraded_to_paid->year][$client->upgraded_to_paid->month])) {
                    $metrics[$client->upgraded_to_paid->year][$client->upgraded_to_paid->month] = 0;
                }

                $metrics[$client->upgraded_to_paid->year][$client->upgraded_to_paid->month] += array_get($plans, $client->upgraded_plan_id, 0);
                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric
        $this->writeMetrics($metrics, Metric::MRR_NEW_FROM_ACCOUNT_EXPANSIONS, 3, false, true);
        $this->info('Get new MRR from account expansions ended.');
    }

    /**
     * Get the sum that we have lost on client plan unsubscribed
     */
    protected function getLostMRR()
    {
        $metrics = [];
        $query = ClientContracts::select('updated_at', 'service_plan_id')
            ->where('status', 5)
            ->orderBy('updated_at', 'asc');

        $this->info('Get lost MMR started.');
        $bar = $this->output->createProgressBar($query->count());

        // Get plans price
        $plans = ServicePlans::pluck('price', 'id');

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $plans, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->updated_at->year])) {
                    $metrics[$client->updated_at->year] = [];
                }
                if (empty($metrics[$client->updated_at->year][$client->updated_at->month])) {
                    $metrics[$client->updated_at->year][$client->updated_at->month] = 0;
                }

                $metrics[$client->updated_at->year][$client->updated_at->month] -= array_get($plans, $client->service_plan_id, 0);
                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric
        $this->writeMetrics($metrics, Metric::MRR_LOST, 3, false, true);
        $this->info('Get lost MMR ended.');
    }

    protected function getMRREndOfTheMonth()
    {
        $metrics = [];
        $PluginClients = [];

        $query = ClientPayments::where('transfer_id', '!=', null);

        if($this->pluginMagento == true) {
            $PluginClient = ApiRequestLogs::select('created_at', 'client_id')->where('plugin_type', 'magento')->where('error', 'not like', 'Free%')->groupby('client_id')->orderby('created_at', 'asc')->get();

            foreach($PluginClient as $MClient) {
                $PluginClients[] = $MClient->client_id;

            }

            $query = ClientPayments::where('transfer_id', '!=', null)->whereIn('client_id', $PluginClients);

        }
        if($this->pluginWoocommerce == true) {
            $PluginClient = ApiRequestLogs::select('created_at', 'client_id')->where('plugin_type', 'woocommerce')->where('error', 'not like', 'Free%')->groupby('client_id')->orderby('created_at', 'asc')->get();

            foreach($PluginClient as $WClient) {
                $PluginClients[] = $WClient->client_id;

            }

            $query = ClientPayments::where('transfer_id', '!=', null)->whereIn('client_id', $PluginClients);

        }

        $this->info('Get MRR End of the Month Started');
        $bar = $this->output->createProgressBar($query->count());

        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $metrics[$client->created_at->year][$client->created_at->month] += $client->amount;
                $bar->advance();
            }
        });
        $bar->finish();

        if($this->pluginMagento == false && $this->pluginWoocommerce == false) {
            $this->writeMetrics($metrics, Metric::MRR_END_OF_THE_MONTH, 3, false, true);
        }

        if($this->pluginMagento == true) {
            $this->writePluginMetrics('magento', $metrics, PluginMetrics::MRR_END_OF_THE_MONTH, 3);
        }
        if($this->pluginWoocommerce == true) {
            $this->writePluginMetrics('woocommerce', $metrics, PluginMetrics::MRR_END_OF_THE_MONTH, 3);
        }
        $this->info('Get MRR end of the month.');

    }

    protected function getMRRBeginningOfTheMonth() {
        $metrics = [];

        $query = ClientPayments::where('transfer_id', '!=', null);

        $this->info('Get MRR Beginning Of The Month Started');
        $bar = $this->output->createProgressBar($query->count());

        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $metrics[$client->created_at->year][$client->created_at->month] += $client->amount;
                $bar->advance();
            }
        });
        $bar->finish();

        $this->writeMetrics($metrics, Metric::MRR_BEGINNING_OF_THE_MONTH, 3, true, true);
        $this->info('Get MRR Beginning of the month.');
    }

    /**
     * Gets track count for every month
     */
    protected function getTrackingNumbers()
    {
        $metrics = [];
        $query = TrackLog::where('created_at', '>', '2016-01-01');

        $this->info('Get tracking numbers started.');
        $bar = $this->output->createProgressBar($query->count());

        // Format query and split by year/month
        $query->chunk(200, function ($clients) use (&$metrics, $bar) {
            foreach ($clients as $client) {
                if (empty($metrics[$client->created_at->year])) {
                    $metrics[$client->created_at->year] = [];
                }
                if (empty($metrics[$client->created_at->year][$client->created_at->month])) {
                    $metrics[$client->created_at->year][$client->created_at->month] = 0;
                }

                $metrics[$client->created_at->year][$client->created_at->month] += 1;
                $bar->advance();
            }
        });

        $bar->finish();

        // Update or create metric
        $this->writeMetrics($metrics, Metric::TRACKING_NUMBERS, 4);
        $this->info('Get tracking numbers finished.');
    }



    protected function writeMetrics($metrics, $metric, $type, $beginning = false, $dollars = false)
    {
        foreach ((array) $metrics as $year => $months) {
            foreach ($months as $month => $count) {
                if($beginning == true) {
                    $month++;
                    if ($month == 13) {
                        $year++;
                        $month = 1;
                    }
                }
                $dt = Carbon::now();
                if($dollars) {
                    if($month == $dt->month && $year == $dt->year) {
                        Metric::updateOrCreate(
                            [
                                'metric' => $metric,
                                'type' => $type,
                                'year' => $year,
                                'month' => $month,
                            ],
                            ['count' => 0]
                        );
                    } else {
                        Metric::updateOrCreate(
                            [
                                'metric' => $metric,
                                'type' => $type,
                                'year' => $year,
                                'month' => $month,
                            ],
                            ['count' => $count / 100]
                        );
                    }
                }
                if($dollars == false) {
                    if($month == $dt->month && $year == $dt->year) {
                        Metric::updateOrCreate(
                            [
                                'metric' => $metric,
                                'type' => $type,
                                'year' => $year,
                                'month' => $month,
                            ],
                            ['count' => 0]
                        );
                    } else {
                        Metric::updateOrCreate(
                            [
                                'metric' => $metric,
                                'type' => $type,
                                'year' => $year,
                                'month' => $month,
                            ],
                            ['count' => $count]
                        );
                    }
                }
            }
        }
    }

    protected function writePluginMetrics($plugin, $metrics, $metric, $type, $beginning = false) {
        foreach ((array) $metrics as $year => $months) {
            foreach ($months as $month => $count) {
                if($beginning == true) {
                    $month++;
                    if ($month == 13) {
                        $year++;
                        $month = 1;
                    }
                }
                PluginMetrics::updateOrCreate(
                    [
                        'plugin' => $plugin,
                        'metric' => $metric,
                        'type' => $type,
                        'year' => $year,
                        'month' => $month,
                    ],
                    ['count' => $count]
                );
            }
        }
    }
}