<?php
namespace App\Components;

use Illuminate\Database\Eloquent\Collection;

class Metrics
{
    private $metrics;
    private $formatedMetrics = [];

    /**-
     * Metrics constructor.
     * @param Collection $metrics
     */
    function __construct(Collection $metrics)
    {
        $this->metrics = $metrics;
    }

    /**
     * Format metrics array
     *
     * @return array
     */
    public function formatMetrics()
    {
        $this->formatedMetrics = [];

        foreach ($this->metrics as $metric) {
            // Assign all month, to simplify response
            // run only one time
            if (empty($this->formatedMetrics[$metric->type][$metric->metric])) {
                $this->formatedMetrics[$metric->type][$metric->metric] = $this->setMonths();
            }

            $this->formatedMetrics[$metric->type][$metric->metric][$metric->month] = $metric->count;
            $this->formatedMetrics[$metric->type][$metric->metric]['total'] += $metric->count;
        }

        // Additional countings for type 1
        if(!empty($this->formatedMetrics[1]['total_new_signups'])) {
            $this->formatedMetrics[1]['growth new signups'] = $this->formatGrowth($this->formatedMetrics[1]['total_new_signups']);
        }

        // Additional countings for type 2
        if(!empty($this->formatedMetrics[2]['New customers']) && !empty($this->formatedMetrics[1]['total_new_signups'])) {
            $this->formatedMetrics[2]['Conversion rate'] = $this->formatConversionRate($this->formatedMetrics[2]['New customers'],
                $this->formatedMetrics[1]['total_new_signups']);
        }

        if(!empty($this->formatedMetrics[2]['New customers']) && !empty($this->formatedMetrics[2]['Lost customers'])) {
            $this->formatedMetrics[2]['Net new customers'] = $this->formatSum($this->formatedMetrics[2]['New customers'],
                $this->formatedMetrics[2]['Lost customers']);
        }

//        if(!empty($this->formatedMetrics[2]['Customers beginning of the month']) && !empty($this->formatedMetrics[2]['Net new customers'])) {
//            $this->formatedMetrics[2]['Customers end of the month'] = $this->formatSum($this->formatedMetrics[2]['Customers beginning of the month'], $this->formatedMetrics[2]['Net new customers']);
//        }

        if(!empty($this->formatedMetrics[2]['Customers end of the month'])) {
            $this->formatedMetrics[2]['m/m growth customers'] = $this->formatGrowth($this->formatedMetrics[2]['Customers end of the month']);
        }

        // Additional countings for type 3
        if(!empty($this->formatedMetrics[3]['New MRR from new customers']) && !empty($this->formatedMetrics[3]['New MRR from account expansions'])) {
            $this->formatedMetrics[3]['Total new MRR'] = $this->formatSum($this->formatedMetrics[3]['New MRR from new customers'],
                $this->formatedMetrics[3]['New MRR from account expansions']);
        }

        if(!empty($this->formatedMetrics[3]['Lost MRR']) && !empty($this->formatedMetrics[3]['MRR beginning of the month'])) {
            $this->formatedMetrics[3]['MRR churn rate'] = $this->formatChurn($this->formatedMetrics[3]['Lost MRR'],
                $this->formatedMetrics[3]['MRR beginning of the month']);
        }

        if(!empty($this->formatedMetrics[3]['Total new MRR']) && !empty($this->formatedMetrics[3]['Lost MRR'])) {
            $this->formatedMetrics[3]['Net new MRR'] = $this->formatSum($this->formatedMetrics[3]['Total new MRR'],
                $this->formatedMetrics[3]['Lost MRR']);
        }

        if(!empty($this->formatedMetrics[3]['MRR end of the month'])) {
            $this->formatedMetrics[3]['m/m growth MRR'] = $this->formatGrowth($this->formatedMetrics[3]['MRR end of the month']);
        }
//        if(!empty($this->formatedMetrics[2]['Customers beginning of the month'])) {
//            $this->formatedMetrics[2]['Customers beginning of the month'] = $this->formatGrowth($this->formatedMetrics[2]['Customers beginning of the month']);
//        }

        return $this->formatedMetrics;
    }

    /**
     * Format an array for conversion rate for all months
     * total also counts
     *
     * @param $metrics1
     * @param $metrics2
     * @return array
     */
    public function formatConversionRate($metrics1, $metrics2)
    {
        $conversionRate = [];
        unset($metrics1['total']);

        foreach ($metrics1 as $month => $result) {
            if (empty($conversionRate)) {
                $conversionRate = $this->setMonths();
            }

            $previousMonth = (int)$month - 1;
            if ($previousMonth > 0) {
                $conversionRate[$month] = $this->getConversionRate(
                    $result,
                    $metrics2[$previousMonth]
                );


                $conversionRate['total'] += $conversionRate[$month];
            }
        }

        return $conversionRate;
    }


    /**
     * Format an array for growth for all months
     * total also counts
     *
     * @param $metrics
     * @return array
     */
    public function formatGrowth($metrics)
    {
        $growth = [];
        unset($metrics['total']);

        foreach ($metrics as $month => $result) {
            if (empty($growth)) {
                $growth = $this->setMonths();
            }

            $previousMonth = (int)$month - 1;
            if ($previousMonth > 0) {
                $growth[$month] = $this->getGrowth(
                    $result,
                    $metrics[$previousMonth]
                );

                $growth['total'] += $growth[$month];
            }
        }

        return $growth;
    }

    /**
     * Format an array for sum for all months
     * total also counts
     *
     * @param $metrics1
     * @param $metrics2
     * @return array
     */
    public function formatSum($metrics1, $metrics2)
    {
        $sum = [];
        unset($metrics1['total']);

        foreach ($metrics1 as $month => $result) {
            if (empty($sum)) {
                $sum = $this->setMonths();
            }

            $sum[$month] = $this->getSum(
                $result,
                $metrics2[$month]
            );

            $sum['total'] += $sum[$month];

        }

        return $sum;
    }

    /**
     * Format an array for churn for all months
     * total also counts
     *
     * @param $metrics1
     * @param $metrics2
     * @return array
     */
    public function formatChurn($metrics1, $metrics2)
    {
        $churnRate = [];
        unset($metrics1['total']);

        foreach ($metrics1 as $month => $result) {
            if (empty($churnRate)) {
                $churnRate = $this->setMonths();
            }

            $churnRate[$month] = $this->getChurnRate(
                $result,
                $metrics2[$month]
            );

            $churnRate['total'] += $churnRate[$month];

        }

        return $churnRate;
    }
    /**
     * Set month array with zero values
     * also added total key with zero value
     * This is depending on the view layout
     *
     * @return array
     */
    private function setMonths()
    {
        $months = [];

        for ($i = 1; $i <= 12; $i++) {
            $months[$i] = 0;
        }
        $months['total'] = 0;

        return $months;
    }

    /**
     * Existing month result is divided by previous month result
     *
     * @param $thisParam
     * @param $previousParam
     * @return float
     */
    private function getGrowth($thisParam, $previousParam)
    {
        $growth= 0;

        if (! empty($thisParam) && ! empty($previousParam)) {
            $growth = (($thisParam / $previousParam) - 1) * 100;
        }

        return round($growth, 2);
    }

    /**
     * Divide parameters and get percentage
     *
     * @param $firstParam
     * @param $secondParam
     * @return float
     */
    private function getConversionRate($firstParam, $secondParam)
    {
        $conversionRate= 0;

        if (! empty($firstParam) && ! empty($secondParam)) {
            $conversionRate = ($firstParam / $secondParam) * 100;
        }

        return round($conversionRate, 2);
    }

    /**
     * Divide parameters and get percentage
     *
     * @param $firstParam
     * @param $secondParam
     * @return float
     */
    private function getChurnRate($firstParam, $secondParam)
    {
        $churnRate= 0;

        if (! empty($firstParam) && ! empty($secondParam)) {
            $churnRate = (-$firstParam / $secondParam) * 100;
        }

        return round($churnRate, 2);
    }

    /**
     * Sum parameters
     *
     * @param $firstParam
     * @param $secondParam
     * @return int
     */
    private function getSum($firstParam, $secondParam)
    {
        $sum = 0;

        if (! empty($firstParam) && ! empty($secondParam)) {
            $sum = $firstParam + $secondParam;
        } else if(! empty($firstParam) && empty($secondParam)) {
            $sum = $firstParam;
        } else {
            $sum = $secondParam;
        }

        return $sum;
    }
}